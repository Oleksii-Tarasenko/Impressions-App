FROM python:3.8

RUN useradd --create-home userapp
WORKDIR /impressions_app
RUN pip install -U pipenv
COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy --system
COPY ./ .
RUN chown -R userapp:userapp ./
USER userapp

EXPOSE 8000
CMD gunicorn -w 5 --bind 0.0.0.0:$PORT app:app
