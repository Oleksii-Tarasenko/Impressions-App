from src import db
from src.database.models import Impression, Comment


def populate_impressions():
    """"Popultaing database with test data for impressions"""
    windserfing = Impression(
        title='Windserfing',
        executor='Funsurf',
        description='Windsurfing is a water sport that combines characteristics of both sailing and surfing.The windsurfer uses the wind to propel a board forward under his feet and skim across the water.',
        place="Kiev, Obolon/Troeschina / Kiev's Sea",
        duration=1.0,
        # rating=8.5,
        price=300.0
    )
    plane = Impression(
        title='Piloting the plane',
        executor='Aeropract',
        description='Piloting the plane is an exiting and unusual activity. Try yourself as second-pilot together with experienced certified instructor.',
        place='Kiev region, airfield "Chaika"',
        duration=0.5,
        # rating=9.0,
        price=3000.0
    )
    ballons = Impression(
        title='Balloon flight',
        executor='Montgolfier',
        description='Feel yourself a magician - make a dream of someone special come true with a balloon flight.. That’s one of the most bright and unforgettable impression!',
        place='Kiev region, 45km on Zhytomyr road',
        duration=1.5,
        # rating=7.0,
        price=9000.0
    )
    db.session.add(windserfing)
    db.session.add(plane)
    db.session.add(ballons)

    db.session.commit()
    db.session.close()


def populate_comments():
    """"Popultaing database with test data for comments"""
    print(db.session.query(Impression).filter_by(title='Balloon flight').first().id)
    print(db.session.query(Impression).filter_by(title='Piloting the plane').first().id)
    print(db.session.query(Impression).filter_by(title='Windserfing').first().id)

    wonderful = Comment(
        name='Alex',
        description='Wonderful',
        rating=9.0,
        impression_id=db.session.query(Impression).filter_by(title='Balloon flight').first().id
    )
    exciting = Comment(
        name='Pedro',
        description='Exciting',
        rating=8.0,
        impression_id=db.session.query(Impression).filter_by(title='Piloting the plane').first().id
    )
    good = Comment(
        name='Vova',
        description='Good',
        rating=8.0,
        impression_id=db.session.query(Impression).filter_by(title='Windserfing').first().id
    )

    db.session.add(wonderful)
    db.session.add(exciting)
    db.session.add(good)

    db.session.commit()
    db.session.close()


if __name__ == '__main__':
    print('Populating db...')
    populate_impressions()
    populate_comments()
    populate_comments()
    populate_comments()
    print('Succesfully populated.')
