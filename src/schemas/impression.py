from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested

from src.database.models import Impression


class ImpressionSchema(SQLAlchemyAutoSchema):
    """Impression Schema"""
    class Meta:
        model = Impression
        #exclude = ['id']
        load_instance = True
        include_fk = True
    #comments = Nested('CommentSchema', many=True, exclude=('impressions', ))
