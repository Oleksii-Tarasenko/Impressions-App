from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from src import db
from src.schemas.impression import ImpressionSchema
from src.services.impression_service import ImpressionService


class ImpressionListApi(Resource):
    """Impressions API. Implementation of CRUD operations"""
    impression_schema = ImpressionSchema()

    def get(self, uuid=None):
        """Returns exact impression if it's uuid is given
         or all impressions if no arguments is given"""
        if not uuid:
            impressions = ImpressionService.fetch_all_impressions(db.session).all()
            return self.impression_schema.dump(impressions, many=True), 200
        else:
            impression = ImpressionService.fetch_impression_by_uuid(db.session, uuid)
            if not impression:
                return '', 404
            else:
                return self.impression_schema.dump(impression), 200

    def post(self, data=None):
        """Creates a new impression in database with given data"""
        try:
            if not data:
                impression = self.impression_schema.load(request.json, session=db.session)
            else:
                impression = self.impression_schema.load(data, session=db.session)
        except ValidationError as e:
            return {'msg': str(e)}, 400
        db.session.add(impression)
        db.session.commit()
        return self.impression_schema.dump(impression), 201

    def put(self, uuid, data=None):
        """Updates existing impression in database with given data"""
        impression = ImpressionService.fetch_impression_by_uuid(db.session, uuid)
        if not impression:
            return 'no impression found', 404
        try:
            if not data:
                impression = self.impression_schema.load(request.json, instance=impression, session=db.session)
            else:
                impression = self.impression_schema.load(data, instance=impression, session=db.session)
        except ValidationError as e:
            return {'msg': str(e)}, 400
        db.session.add(impression)
        db.session.commit()
        return self.impression_schema.dump(impression), 200

    def delete(self, uuid):
        """Deletes impression in database with given data"""
        impression = ImpressionService.fetch_impression_by_uuid(db.session, uuid)
        if not impression:
            return '', 204
        db.session.delete(impression)
        db.session.commit()
        return '', 204
