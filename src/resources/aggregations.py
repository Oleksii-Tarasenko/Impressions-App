import numpy as numpy
from flask_restful import Resource
from sqlalchemy import func


from src import db
from src.database.models import Comment
from src.resources.comments import CommentListApi
from src.resources.impressions import ImpressionListApi

"""
class AggregationApi(Resource):
    def get(self):
        impression_count_rating = db.session.query(func.count(Comment.rating)).scalar()
        impression_avg_rating = db.session.query(func.avg(Comment.rating)).scalar()
        return {
            'impression_count_rating': impression_count_rating,
            'impression_avg_rating': impression_avg_rating
        }
"""

class CommentRatingApi(Resource):
    """"Returns average comment's rating for all or for chosen impression"""
    def get(self, impression_id=None):
        if impression_id:
            comments, _ = CommentListApi().get(impression_id=impression_id)
            list_of_ratings = []
            for comment in comments:
                list_of_ratings.append(comment['rating'])
            avg_rating_comments_of_impression = ('%.1f' % numpy.mean(list_of_ratings))
            return {
                'avg_rating_comments_of_impression': avg_rating_comments_of_impression
            }
        else:
            impressions, _ = ImpressionListApi().get()
            list_of_ratings_all_impr = {}
            for impression in impressions:
                impression_id = impression['id']
                comments, _ = CommentListApi().get(impression_id=impression_id)
                list_of_ratings = []
                for comment in comments:
                    list_of_ratings.append(comment['rating'])
                avg_rating_comments_of_impression = ('%.1f' % numpy.mean(list_of_ratings))
                list_of_ratings_all_impr[impression_id] = avg_rating_comments_of_impression
            return list_of_ratings_all_impr
