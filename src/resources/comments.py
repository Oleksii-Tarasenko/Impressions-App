from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from src import db
from src.database.models import Impression, Comment
from src.schemas.comment import CommentSchema
from src.services.comment_service import CommentService


class CommentListApi(Resource):
    """Comments API. Implementation of CRUD operations"""
    comment_schema = CommentSchema()

    def get(self, uuid=None, impression_id=None):
        """Returns exact comment if it's uuid is given
         or list of comments which belongs to exact impression if impression_id is given
         or all impressions if no arguments is given"""
        if not uuid:
            if impression_id:
                comments = CommentService.fetch_comment_by_impression_id(db.session, impression_id).all()
                return self.comment_schema.dump(comments, many=True), 200
            else:
                comments = CommentService.fetch_all_comments(db.session).all()
                return self.comment_schema.dump(comments, many=True), 200
        else:
            comment = CommentService.fetch_comment_by_uuid(db.session, uuid)
            if not comment:
                return '', 404
            else:
                return self.comment_schema.dump(comment), 200

        """
        # alternative method implementation
        query = db.session.query(Comment)
        if uuid:
            query = query.filter_by(uuid=uuid)
        elif impression_id:
            query = query.filter_by(impression_id=impression_id)

        comments = list(query)
        return self.comment_schema.dump(comments, many=True), 200

        """

    def post(self):  # , impression_id):
        """Creates a new comment in database with given data"""
        try:
            comment = self.comment_schema.load(request.json, session=db.session)
        except ValidationError as e:
            return {'msg': str(e)}, 400
        db.session.add(comment)
        db.session.commit()
        return self.comment_schema.dump(comment), 201

        # alternative method implementation
        """impression = db.session.query(Impression).filter_by(id=impression_id).first()
        if not impression:
            return 'no impression found', 404
        else:"""

    def put(self, uuid):
        """Updates existing comment in database with given data"""
        comment = CommentService.fetch_comment_by_uuid(db.session, uuid)
        if not comment:
            return 'no comment found', 404
        try:
            comment = self.comment_schema.load(request.json, instance=comment, session=db.session)
        except ValidationError as e:
            return {'msg': str(e)}, 400
        db.session.add(comment)
        db.session.commit()
        return self.comment_schema.dump(comment), 200

    def delete(self, uuid):
        """Deletes comment in database with given data"""
        comment = CommentService.fetch_comment_by_uuid(db.session, uuid)
        if not comment:
            return '', 204
        db.session.delete(comment)
        db.session.commit()
        return '', 204
