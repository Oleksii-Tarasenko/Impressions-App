"""Dtabase Models classes"""

import uuid
from src import db


class Impression(db.Model):
    """Creating Impression Model"""
    __tablename__ = 'impressions'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    executor = db.Column(db.String(120), nullable=False)
    uuid = db.Column(db.String(36), unique=True)
    description = db.Column(db.Text)
    place = db.Column(db.String(120), nullable=False)
    duration = db.Column(db.Float)
    # rating = db.Column(db.Float)
    price = db.Column(db.Float)
    comments = db.relationship('Comment', back_populates='impressions', cascade="all,delete")

    def __init__(self, title, executor, description, place, duration, price, comments=None):
        self.title = title
        self.executor = executor
        self.description = description
        self.place = place
        self.duration = duration
        # self.rating = db.session.query(Comment).filter(impression_id=self.id)
        self.price = price
        self.uuid = str(uuid.uuid4())
        if not comments:
            self.comments = []
        else:
            self.comments = comments

    def __repr__(self):
        return f'Impression({self.id}, {self.title}, {self.uuid}, {self.place}, ' \
               f'{self.executor}, {self.price})'


class Comment(db.Model):
    """Creating Comment Model"""
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    uuid = db.Column(db.String(36), unique=True)
    description = db.Column(db.Text)
    rating = db.Column(db.Float)
    # created = db.Column(db.DateTime, default=datetime.now())
    impressions = db.relationship('Impression', back_populates='comments')
    impression_id = db.Column(db.Integer, db.ForeignKey('impressions.id'))

    def __init__(self, name, description, rating, impression_id):
        self.name = name
        self.description = description
        self.rating = rating
        self.uuid = str(uuid.uuid4())
        self.impression_id = impression_id

    def __repr__(self):
        return f'Comment({self.impression_id},{self.name}, {self.uuid}, ' \
               f'{self.description}, {self.rating})'
